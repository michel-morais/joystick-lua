# Joystick Emulator KeyBoard and mouse using Lua as script for Windows

This project is used to emulate keyboard and mouse using joystick.
The tests were made using PS2 and PS3 joystick controller USB model.
It is possible to create file using Lua as interpreter.
It has calibration screen, discovery keys (keyboard) and some examples of LUA script.
Also is possible just use the emulator as mouse PC controller.

## Links

* Project Page 			- https://bitbucket.org/Michel-braz-morais/joystick-emulator-lua
* pluswindows interface - https://bitbucket.org/Michel-braz-morais/pluswindows
* Lua 					- https://www.lua.org/


## Basic usage

### The Lua script code

```lua

function onInfoJoystick(player, maxNumberButton, name, extraInfo)
    print('onInfoJoystick')
    print('player:',player)
    print('Max button(s):',maxNumberButton)
    print('Name:',name)
    print(extraInfo)
end

function onKeyDownJoystick(player, key)
	if key == 15 or key == 3 then -- 15 on PS2 is X, 3 on PS3 is X
		click('left','d')
	end
end

function onKeyUpJoystick(player, key)
	if key == 15 or key == 3 then -- 15 on PS2 is X, 3 on PS3 is X
		click('left','u')
	end
end

function onMoveJoystick(player, lx, ly, rx, ry)
    moveMouse(lx,ly,true)
end

```

#### Recognition screen
![Scheme](images/print-1.png)

![Scheme](images/print-2.png)

![Scheme](images/print-3.png)

#### Main Menu
![Scheme](images/print-4.png)

### Available lua script for games

> Don't starve

> Tobias and the dark sceptres

### Available lua script for examples

> Getting general information (just print in real time the status of joystick controller)

> Sample script

## License

Joystick Emulator under LUA is under MIT License (MIT).

The MIT License (MIT)

Copyright (c) 2017 [Michel Braz de Morais](michel.braz.morais@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

        
          