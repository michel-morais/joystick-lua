﻿/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| Copyright (C) 2015      by Michel Braz de Morais  <michel.braz.morais@gmail.com>                                       |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|                                                                                                                        |
|-----------------------------------------------------------------------------------------------------------------------*/

#include "joystick.lua.h"
#include <Windows.h>
#include <tchar.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <hidsdi.h>
#include <iostream>
#include <vector>
#include <io.h>
#include "defaultThemePlusWindows.h"

#pragma comment(lib,"hid.lib")

#define SAFE_FREE(p)	{ if(p) { HeapFree(hHeap, 0, p); (p) = NULL; } }
#define CHECK(exp)		{ if(!(exp)) goto Error; }
#define STRING_VAR_ARG(...) #__VA_ARGS__

using namespace std;

#pragma comment (lib, "lua-5.2.2.lib")

extern "C"
{
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}


JOYSTICK_LUA::JOYSTICK_LUA()
{
	this->lua = NULL;
	this->idTryAbout = -1;
	this->idTryExit  = -1;
	this->idTryGettingInfo = -1;
	this->idTryConsole = -1;
	this->idTrySampleDontStarve = -1;
	this->idTrySampleGraph = -1;
	this->idTryOpenFile = -1;
	this->idTrySampleTobias = -1;
    this->idTryExample = -1;
	this->idTryDiscoverKeys = -1;
	this->indexSelectedJoystick = 0;
	this->modeDiscoverKey = false;
	JOYSTICK_LUA::myInstance = this;
    whiteBrush  =   this->createBrush(255,255,255);
    blueBrush   =   this->createBrush(0,0,255);
    redBrush    =   this->createBrush(255,0,0);
    penBlack    =   this->createPen(0,0,0,0,2);
    penRed      =   this->createPen(255,0,0,0,0);
    penBlue     =   this->createPen(0,0,255,0,0);
    fprintf(stderr, "LUA: %s\n", LUA_VERSION);
}

JOYSTICK_LUA::~JOYSTICK_LUA()
{
	if (this->lua)
		lua_close(this->lua);
	for (unsigned int i = 0; i < this->lsInfoJoystick.size(); i++)
	{
		INFO_JOYSTICK* pJoystick = this->lsInfoJoystick[i];
		delete pJoystick;
	}
	this->lsInfoJoystick.clear();

    for (unsigned int i = 0; i < this->lsTimerCallBack.size(); i++)
    {
        USER_DATA_TIMER* userData = this->lsTimerCallBack[i];
        this->window.killTimer(userData->idTimer);
        delete userData;
    }
    this->lsTimerCallBack.clear();

    this->release(whiteBrush);
    this->release(blueBrush);
    this->release(redBrush);
    this->release(penBlack);
}

bool JOYSTICK_LUA::render(mbm::COMPONENT_INFO & component)
{
    this->setBrush(this->whiteBrush);
    this->drawRectangle(component.rect);

    this->setPen(this->penBlack);
    if (this->indexSelectedJoystick < this->lsInfoJoystick.size())
	{
		
		INFO_JOYSTICK* pInfoJoystick = this->lsInfoJoystick[indexSelectedJoystick];
		//all Devices
		int numDeviceValid = 0;
		for (unsigned int i = 0; i < this->lsInfoJoystick.size(); i++)
		{
			INFO_JOYSTICK* ptrInfoJoystick = this->lsInfoJoystick[i];
			if (ptrInfoJoystick->hDevice.dwType == RIM_TYPEHID)
			{
				int x = numDeviceValid * 100;
				char str[255];
				numDeviceValid++;
                
                this->setBrush(this->whiteBrush);
				this->drawRectangle(20 + x, 400, 90, 30);
				sprintf(str, "Player %d", numDeviceValid);
				if (this->indexSelectedJoystick == i)
				{
                    this->setBrush(this->redBrush);
					this->drawRectangle(20 + x, 400, 90, 30);
					this->drawText(20 + x + 20, 400 + 7, 0, 255, 255, str, true);
				}
				else
				{
					this->drawText(20 + x + 20, 400 + 7, 0, 0, 255, str, true);
				}
					
			}
		}

		for (int i = 0; i < pInfoJoystick->numberOfButtons; i++)
			this->DrawButton(i + 1, 20 + i * 40, 20, pInfoJoystick->bButtonStates[i]);
		//analog
		this->DrawCrosshair(20, 100, pInfoJoystick->lAxisX, pInfoJoystick->lAxisY);
		this->DrawCrosshair(296, 100, pInfoJoystick->lAxisZ, pInfoJoystick->lAxisRz);

        //Pad pressed
        this->setPen(this->penBlack);
        this->setBrush(this->whiteBrush);
		this->drawRectangle(572, 100, 256, 256);
		
        //Pad empty
        this->setPen(this->penRed);
        this->setBrush(this->whiteBrush);
		for (unsigned int i = 0; i < 8; i++)
		{
			unsigned int centerX = 148 + 276 + 276;
			unsigned int centerY = 228;
			int xPos = (int)(sin(-2 * M_PI * i / 8 + M_PI) * 100.0);
			int yPos = (int)(cos(2 * M_PI * i / 8 + M_PI) * 100.0);
			this->drawCircle(centerX + xPos, yPos + centerY, 8);
		}

		this->setPen(this->penRed);
        this->setBrush(this->redBrush);
		if (pInfoJoystick->lHat >= 0 && pInfoJoystick->lHat <= 8)
		{
			unsigned int centerX = 148 + 276 + 276;
			unsigned int centerY = 228;
			int xPos = (int)(sin(-2 * M_PI * pInfoJoystick->lHat / 8 + M_PI) * 100.0);
			int yPos = (int)(cos(2 * M_PI * pInfoJoystick->lHat / 8 + M_PI) * 100.0);
			this->drawCircle(centerX + xPos, yPos + centerY, 5);
		}
	}
    return true;
}
	
int JOYSTICK_LUA::loop()
{
	if (this->window.isLoaded())
	{
		return this->window.enterLoop(NULL);
	}
	else
	{
		MSG msg;
		while (GetMessage(&msg, NULL, 0, 0))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		return (int)msg.wParam;
	}
}
	
bool JOYSTICK_LUA::init(const bool createSampleWindow, DWORD IDI_ICON, const char* fileToLoad, const char* _nameApp )
{
	this->nameExe = _nameApp ? _nameApp : "JoystickLua.exe";
	const char* nameApp = "Keyboard/mouse emulator by joystick";
	this->window.setObjectContext(this,0);
	this->window.exitOnEsc = false;
	this->window.askOnExit = false;
	if (createSampleWindow)
	{
        THEME_WINPLUS_CUSTOM_RENDER::setTheme(2, true);
		if (this->window.init(nameApp, 900, 500, 0, 0, false, false, false, false,nullptr, false,IDI_ICON))
		{
            RECT rect = this->window.getRect();
            const int width = rect.right - rect.left - 2;
            const int height = rect.bottom - rect.top - 2;
            int id = this->window.addGroupBox(nullptr,1,1,width,height,-1,nullptr);
			if (this->window.setOnParserRawInput(onParseRawInput) &&
				this->updateDevices())
			{
				this->window.hideConsoleWindow();
				this->window.setOnClickLeftMouse(onMouseEvent);
                
                this->window.setDrawer(this,id);
				fprintf(stderr, "Dispositivo(s) encontrado(s): %ud\n", this->getTotalDevices());
				return this->createSceneLua(fileToLoad);
			}
		}
	}
	else
	{
		if (this->window.init(nameApp, 400, 400, 0, 0, false, false, false, false, nullptr, false, IDI_ICON))
		{
			if (this->window.setOnParserRawInput(onParseRawInput) &&
				this->updateDevices())
			{
				this->window.hide();
				this->window.hideConsoleWindow();
				int myId = 0;
				int idTry = 0;
				if (JOYSTICK_LUA::isPt())
				{
					idTry = this->window.addTrayIcon(IDI_ICON, onOptTryIcon, nameApp);
					this->idTryOpenFile = this->window.addMenuTrayIcon("Abrir Arquivo LUA...", idTry, false, myId++, true);

					int idSubMenu = this->window.addSubMenuTrayIcon("Scripts", myId++);
					this->idTryGettingInfo = this->window.addMenuTrayIcon("Obter informações em geral!", idSubMenu, false, 0, false);
                    this->idTrySampleDontStarve = this->window.addMenuTrayIcon("Game Dont starve", idSubMenu, false, 1, false);
					this->idTrySampleTobias = this->window.addMenuTrayIcon("Tobias and the dark sceptres", idSubMenu, false, 2, false);
                    this->idTryExample = this->window.addMenuTrayIcon("Script de exemplo!", idSubMenu, false, 3, false);

					this->idTrySampleGraph = this->window.addMenuTrayIcon("Calibração", idTry, false, myId++, false);
					this->idTryDiscoverKeys = this->window.addMenuTrayIcon("Descobrir teclas", idTry, false, myId++, false);
					this->idTryConsole = this->window.addMenuTrayIcon("Mostrar console de log(s)", idTry, false, myId++, false);
					this->idTryAbout = this->window.addMenuTrayIcon("Sobre!", idTry, false, myId++, false);

					this->idTryExit = this->window.addMenuTrayIcon("Sair!", idTry, false, myId++, false);
					fprintf(stderr, "Dispositivo(s) encontrado(s): %ud\n", this->getTotalDevices());
					return this->createSceneLua(fileToLoad);
				}
				else
				{
					idTry = this->window.addTrayIcon(IDI_ICON, onOptTryIcon, nameApp);
					this->idTryOpenFile = this->window.addMenuTrayIcon("Open File LUA...", idTry, false, myId++, true);

					int idSubMenu = this->window.addSubMenuTrayIcon("Scripts", myId++);
					this->idTryGettingInfo = this->window.addMenuTrayIcon("Getting general information!", idSubMenu, false, 0, false);
					this->idTrySampleDontStarve = this->window.addMenuTrayIcon("Game Dont starve", idSubMenu, false, 1, false);
					this->idTrySampleTobias = this->window.addMenuTrayIcon("Tobias and the dark sceptres", idSubMenu, false, 2, false);
                    this->idTryExample = this->window.addMenuTrayIcon("Sample script!", idSubMenu, false, 3, false);

					this->idTrySampleGraph = this->window.addMenuTrayIcon("Calibration", idTry, false, myId++, false);
					this->idTryDiscoverKeys = this->window.addMenuTrayIcon("Discover keys", idTry, false, myId++, false);
					this->idTryConsole = this->window.addMenuTrayIcon("Show log(s) console", idTry, false, myId++, false);
					this->idTryAbout = this->window.addMenuTrayIcon("About!", idTry, false, myId++, false);

					this->idTryExit = this->window.addMenuTrayIcon("Exit!", idTry, false, myId++, false);
					fprintf(stderr, "Device (s) found (s): %ud\n", this->getTotalDevices());
					return this->createSceneLua(fileToLoad);
				}
			}
		}
	}
	return false;
}

const bool JOYSTICK_LUA::isPt()
{
	static bool myLanguageFouded = false;
	static bool ispPtBr  = false;
	if (myLanguageFouded == false)
	{
		myLanguageFouded = true;
		WCHAR localeName[LOCALE_NAME_MAX_LENGTH] = { 0 };
		const int len = sizeof(localeName) / sizeof(*(localeName));
		int ret = GetUserDefaultLocaleName(localeName, len);
		if (ret == 0)
			return false;
		const int isPtBr = _wcsnicmp(localeName, L"PT", 2);//pt-BR
		ispPtBr = (isPtBr == 0);
	}
	return ispPtBr;
}
	
void JOYSTICK_LUA::DrawCrosshair(int x, int y, LONG xVal, LONG yVal)
{
    this->setPen(this->penBlack);
    this->setBrush(this->whiteBrush);
	this->drawRectangle(x, y, 256, 256);
	const int px = x + xVal + 128;
	const int py = y + yVal + 128;
    this->setPen(this->penBlue);
    this->setBrush(this->blueBrush);
	this->drawCircle(px, py, 5);
}
	
const bool JOYSTICK_LUA::createSceneLua(const char* fileToLoad)
{
	if (this->lua)
		lua_close(this->lua);
	this->lua = NULL;
	this->lua = luaL_newstate();
	if (this->lua == NULL)
		return false;
	luaL_openlibs(this->lua);
	registerFunctionsLuaInterface();
	const char*  dest = fileToLoad ? fileToLoad : "joystick.lua";
	bool existFile = false;
	if (access(dest, 0) != -1)
		existFile = true;
	char str[255] = "";
	if (JOYSTICK_LUA::isPt())
		sprintf(str, "Arquivo executando!\n %s", dest);
	else
		sprintf(str, "File executing!\n %s", dest);
	if (luaL_dofile(this->lua, dest))
	{
		this->window.hide();
		this->window.showConsoleWindow();
		fprintf(stderr,"%s", luaL_checkstring(lua, -1));
		if (!existFile && strcmp(dest, "joystick.lua") == 0)
		{
			const char* question = "Arquivo\n [joystick.lua] não existe!\n Deseja criar?";
			if (!this->isPt())
				question = "File\n [joystick.lua] doesn't exist!\n Wish create?";
			if (this->window.messageBoxQuestion(question))
			{
				std::string fileName = this->openFileSample("joystick.lua", this->getSampleGettingInformation());
                if(fileName.size())
                {
				    if (luaL_dofile(this->lua, dest))
					    return false;
				    this->window.hideConsoleWindow();
				    this->window.showBallonTrayIcon("Joystick LUA", str, 20);
				    return true;
                }
			}
		}
		return false;
	}
	this->window.showBallonTrayIcon("Joystick LUA", str, 20);
	return true;
}
	
int JOYSTICK_LUA::__luaB_print(lua_State *lua)
{
	const int n = lua_gettop(lua);  /* number of arguments */
	lua_getglobal(lua, "tostring");
	for (int i = 1; i <= n; i++)
	{
		size_t l;
		lua_pushvalue(lua, -1);  /* function to be called */
		lua_pushvalue(lua, i);   /* value to print */
		lua_call(lua, 1, 1);
		const char* s = lua_tolstring(lua, -1, &l);  /* get result */
		if (s == NULL)
			return luaL_error(lua, LUA_QL("tostring") " must return a string to " LUA_QL("print"));
		if (i>1)
			fwrite("\t", sizeof(char), sizeof("\t"), stdout);
		fwrite(s, sizeof(char), l, stdout);
		lua_pop(lua, 1);  /* pop result */
	}
	fwrite("\n", sizeof(char), sizeof("\n"), stdout);
	return 0;
}
	
int JOYSTICK_LUA::moveMouseLua(lua_State *lua)
{
	const int n = lua_gettop(lua);
	if (n >= 2)
	{
		int x = luaL_checkint(lua, 1);
		int y = luaL_checkint(lua, 2);
		int relative = n > 2 ? lua_toboolean(lua, 3) : 1;
		if (relative)
		{
			POINT posMouse;
			GetCursorPos(&posMouse);
			SetCursorPos(posMouse.x + x, posMouse.y + y);
		}
		else
		{
			SetCursorPos(x, y);
		}
	}
	else
	{
		if (JOYSTICK_LUA::isPt())
			return luaL_error(lua, "esperado: moveMouse(x,y,relative=true) ");
		return luaL_error(lua, "expected: moveMouse(x,y,relative=true) ");
	}
	return 0;
}
	
void JOYSTICK_LUA::onTimerOver(WINDOW *window, DATA_EVENT &dataEvent)	
{
    if(dataEvent.userDrawer)
    {
        USER_DATA_TIMER* userData = dynamic_cast<USER_DATA_TIMER*>(dataEvent.userDrawer);
        if(userData)
            JOYSTICK_LUA::myInstance->processTimer(window, userData->functionCallBack);
    }
}
	
void JOYSTICK_LUA::processTimer(WINDOW* window, int callBackTimer)
{
	lua_rawgeti(lua, LUA_REGISTRYINDEX, callBackTimer);
	if (lua_isfunction(this->lua, -1))
	{
		if (lua_pcall(this->lua, 0, 0, 0))
		{
			fprintf(stderr, "[%d] ->\n[%s]", callBackTimer, luaL_checkstring(this->lua, -1));
		}
	}
	else
	{
		lua_pop(this->lua, 1);
	}
}
	
int JOYSTICK_LUA::addTimerLua(lua_State *lua)
{
	JOYSTICK_LUA* j = JOYSTICK_LUA::myInstance;
	const int n = lua_gettop(lua);
	if (n == 2)
	{
        int refCallBack = LUA_NOREF;
        const int tType = lua_type(lua, 1);
        unsigned int miliSeconds = luaL_checkunsigned(lua, 2);
        if(LUA_TFUNCTION == tType)
        {
            lua_pushvalue(lua, 1);
            refCallBack = luaL_ref(lua, LUA_REGISTRYINDEX);
        }
        else if(LUA_TSTRING == tType)
        {
            const char *functionName = lua_tostring(lua, 1);
            lua_getglobal(lua, functionName);
            refCallBack = luaL_ref(lua, LUA_REGISTRYINDEX);
        }
        else
	    {
		    if (JOYSTICK_LUA::isPt())
			    return luaL_error(lua, "esperado: addTimer(function functionCallBack,number millisecond)");
		    return luaL_error(lua, "expected: addTimer(function functionCallBack,number millisecond)");
	    }

        lua_settop(lua, 0);
		
		int ret = -1;
		
        USER_DATA_TIMER* userDataTime = new USER_DATA_TIMER();
		userDataTime->functionCallBack = refCallBack;
        j->lsTimerCallBack.push_back(userDataTime);
        ret = j->window.addTimer(miliSeconds, onTimerOver, userDataTime);
		if (ret == -1)
		{
			if (JOYSTICK_LUA::isPt())
				fprintf(stderr, "Erro ao adicionar o timer");
			else
				fprintf(stderr, "Error adding timer");
		}
        else
        {
            userDataTime->idTimer = ret;
        }
	}
	else
	{
		if (JOYSTICK_LUA::isPt())
			return luaL_error(lua, "esperado: addTimer(function functionCallBack,number millisecond)");
		return luaL_error(lua, "expected: addTimer(function functionCallBack,number millisecond)");
	}
	return 0;
}
	
int JOYSTICK_LUA::clickLua(lua_State *lua)
{
	const int n = lua_gettop(lua);
	if (n >= 2)
	{
        int key = 0;
        const int t = lua_type(lua,1);
		if(t == LUA_TSTRING)
        {
            const char* keyAsString = luaL_checkstring(lua, 1);
            if(strcmpi(keyAsString,"left") == 0)
                key = 1;
            else if(strcmpi(keyAsString,"right") == 0)
                key = 2;
            else if(strcmpi(keyAsString,"middle") == 0)
                key = 3;
            else
            {
		        if (JOYSTICK_LUA::isPt())
			        return luaL_error(lua, "esperado: click(key,['ud']) key -> ['left' ou 1,'right' ou 2, 'middle' ou 3]");
		        return luaL_error(lua, "expected: click(key,['ud']) key -> ['left' or 1,'right' or 2, 'middle' or 3]");
	        }
        }
        else if(t == LUA_TNUMBER)
        {
            key = luaL_checkint(lua, 1);
        }
		const char* type = luaL_checkstring(lua, 2);
		switch (key)
		{
			case 1:
			{
				if (strcmpi(type, "ud") == 0)
					mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
				else if (type[0] == 'u')
					mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
				else if (type[0] == 'd')
					mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
			}
			break;
			case 2:
			{
				if (strcmpi(type, "ud") == 0)
					mouse_event(MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
				else if (type[0] == 'u')
					mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
				else if (type[0] == 'd')
					mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
			}
			break;
			case 3:
			{
				if (strcmpi(type, "ud") == 0)
					mouse_event(MOUSEEVENTF_MIDDLEDOWN | MOUSEEVENTF_MIDDLEUP, 0, 0, 0, 0);
				else if (type[0] == 'u')
					mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, 0);
				else if (type[0] == 'd')
					mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, 0);
			}
			break;
			default:
			{
				if (JOYSTICK_LUA::isPt())
					return luaL_error(lua, "esperado: click(key,['ud']) key -> ['left' ou 1,'right' ou 2, 'middle' ou 3]");
				return luaL_error(lua, "expected: click(key,['ud']) key -> ['left' or 1,'right' or 2, 'middle' or 3]");
			}
		}
	}
	else
	{
		if (JOYSTICK_LUA::isPt())
			return luaL_error(lua, "esperado: click(key,['ud']) key -> ['left' ou 1,'right' ou 2, 'middle' ou 3]");
		return luaL_error(lua, "expected: click(key,['ud']) key -> ['left' or 1,'right' or 2, 'middle' or 3]");
	}
	return 0;
}
	
int JOYSTICK_LUA::scrollLua(lua_State *lua)
{
	const bool zoom = lua_toboolean(lua, 1) ? true : false;
	if (zoom)
		mouse_event(MOUSEEVENTF_WHEEL, 0, 0, DWORD(WHEEL_DELTA), 0);
	else
		mouse_event(MOUSEEVENTF_WHEEL, 0, 0, DWORD(-WHEEL_DELTA), 0);
	return 0;
}

int JOYSTICK_LUA::enableContinousMoveLua(lua_State *lua)
{
	_b_enableContinousMove = lua_toboolean(lua, 1) ? true : false;
	return 0;
}

int JOYSTICK_LUA::sendKeyLua(lua_State *lua)
{
	const int n = lua_gettop(lua);
	if (n >= 1)
	{
		int key = 0;
		if (lua_type(lua, 1) == LUA_TSTRING)
		{
			const char* sKey = lua_tostring(lua, 1);
			int l = strlen(sKey);
			if (l == 1)
			{
				const char letter = sKey[0];
				if (letter >= 32 &&  letter <= 126)
					key = letter;
			}
		}
		else
		{
			key = luaL_checkunsigned(lua, 1);
		}
		if (key)
		{
			const char* type = n > 1 ? luaL_checkstring(lua, 2) : "ud";
			if (strcmpi(type, "ud") == 0)
			{
					
				keybd_event(key, 0, KEYEVENTF_EXTENDEDKEY, 0);//Segura
				keybd_event(key, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);//Solta
			}
			else if (type[0] == 'u' || type[0] == 'U')
			{
				keybd_event(key, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);//Solta
			}
			else if (type[0] == 'd' || type[0] == 'D')
			{
				keybd_event(key, 0, KEYEVENTF_EXTENDEDKEY, 0);//Segura
			}
			else
			{
				if (JOYSTICK_LUA::isPt())
					return luaL_error(lua, "esperado: sendKey(key,['d','u','du'])");
				return luaL_error(lua, "expected: sendKey(key,['d','u','du'])");
			}
		}
	}
	else
	{
		if (JOYSTICK_LUA::isPt())
			return luaL_error(lua,"esperado: sendKey(key,['d','u','du'])" );
		return luaL_error(lua, "expected: sendKey(key,['d','u','du'])");
	}
	return 0;
}
	
bool JOYSTICK_LUA::parseRawInput(PRAWINPUT pRawInput)
{
	PHIDP_PREPARSED_DATA pPreparsedData = NULL;
	HIDP_CAPS            Caps;
	PHIDP_BUTTON_CAPS    pButtonCaps = NULL;
	PHIDP_VALUE_CAPS     pValueCaps = NULL;
	USHORT               capsLength;
	UINT                 bufferSize;
	HANDLE               hHeap = NULL;
	USAGE                usage[MAX_BUTTONS];
	ULONG                usageLength, value;
    INFO_JOYSTICK        infoLastMove;
    bool                 foundJoystick = false;
	hHeap = GetProcessHeap();
	//
	// Get the preparsed data block
	//
	int iIdMyJoystick = -1;
	INFO_JOYSTICK* pInfoJoystick = NULL;
	for (unsigned int i = 0; i < this->lsInfoJoystick.size(); i++)
	{
		pInfoJoystick = this->lsInfoJoystick[i];
		if (pInfoJoystick->hDevice.dwType == RIM_TYPEHID)
		{
			iIdMyJoystick++;
			if (pRawInput->header.hDevice == pInfoJoystick->hDevice.hDevice)
			{
				break;
			}
		}
		else if (pInfoJoystick->hDevice.dwType == RIM_TYPEKEYBOARD && this->modeDiscoverKey)
		{
			if ( pRawInput->header.hDevice == pInfoJoystick->hDevice.hDevice)
			{
				if (pRawInput->data.keyboard.Message == WM_KEYDOWN || pRawInput->data.keyboard.Message == WM_SYSKEYDOWN)
					fprintf(stderr, "\nKey: [%d]", pRawInput->data.keyboard.VKey);
				//its all ok.... just return 
				goto Error;
			}
		}
	}
	if (iIdMyJoystick == -1 || iIdMyJoystick >= (int)this->lsInfoJoystick.size())
		goto Error;
    
    infoLastMove = *pInfoJoystick;

	CHECK(GetRawInputDeviceInfo(pRawInput->header.hDevice, RIDI_PREPARSEDDATA, NULL, &bufferSize) == 0);
	CHECK(pPreparsedData = (PHIDP_PREPARSED_DATA)HeapAlloc(hHeap, 0, bufferSize));
	CHECK((int)GetRawInputDeviceInfo(pRawInput->header.hDevice, RIDI_PREPARSEDDATA, pPreparsedData, &bufferSize) >= 0);

	//
	// Get the joystick's capabilities
	//

	// Button caps
	CHECK(HidP_GetCaps(pPreparsedData, &Caps) == HIDP_STATUS_SUCCESS)
		CHECK(pButtonCaps = (PHIDP_BUTTON_CAPS)HeapAlloc(hHeap, 0, sizeof(HIDP_BUTTON_CAPS)* Caps.NumberInputButtonCaps));

	capsLength = Caps.NumberInputButtonCaps;
	CHECK(HidP_GetButtonCaps(HidP_Input, pButtonCaps, &capsLength, pPreparsedData) == HIDP_STATUS_SUCCESS)
		pInfoJoystick->numberOfButtons = pButtonCaps->Range.UsageMax - pButtonCaps->Range.UsageMin + 1;

	// Value caps
	CHECK(pValueCaps = (PHIDP_VALUE_CAPS)HeapAlloc(hHeap, 0, sizeof(HIDP_VALUE_CAPS)* Caps.NumberInputValueCaps));
	capsLength = Caps.NumberInputValueCaps;
	CHECK(HidP_GetValueCaps(HidP_Input, pValueCaps, &capsLength, pPreparsedData) == HIDP_STATUS_SUCCESS)

		//
		// Get the pressed buttons
		//

	usageLength = pInfoJoystick->numberOfButtons;
	CHECK(
		HidP_GetUsages(
		HidP_Input, pButtonCaps->UsagePage, 0, usage, &usageLength, pPreparsedData,
		(PCHAR)pRawInput->data.hid.bRawData, pRawInput->data.hid.dwSizeHid
		) == HIDP_STATUS_SUCCESS);

	if (!pInfoJoystick->info)
	{
		this->onInfoDevice(
			iIdMyJoystick+1,
			pInfoJoystick->numberOfButtons,
			pInfoJoystick->name.c_str(),
			pInfoJoystick->extraInfo.c_str());
			pInfoJoystick->info = true;
	}
	ZeroMemory(pInfoJoystick->bButtonStates, sizeof(pInfoJoystick->bButtonStates));
	//button Clicked
	for (ULONG i = 0; i < usageLength; i++)
	{
		int key = usage[i] - pButtonCaps->Range.UsageMin;
		pInfoJoystick->bButtonStates[key] = TRUE;
	}

	for (int i = 0; i < pInfoJoystick->numberOfButtons; i++)
	{
		if (pInfoJoystick->bButtonStates[i])
		{
			switch (pInfoJoystick->bStateKey[i])
			{
				case KEY_NONE:
				{
					this->onKeyDown(iIdMyJoystick + 1, i + 1);
					pInfoJoystick->bStateKey[i] = KEY_DOWN;
				}
				break;
				case KEY_DOWN:
				{
				}
				break;
				case KEY_UP:
				{
					this->onKeyDown(iIdMyJoystick + 1, i + 1);
					pInfoJoystick->bStateKey[i] = KEY_DOWN;
				}
				break;
			}
		}
		else
		{
			switch (pInfoJoystick->bStateKey[i])
			{
				case KEY_NONE:
				{
						
				}
				break;
				case KEY_DOWN:
				{
					this->onKeyUp(iIdMyJoystick + 1, i + 1);
					pInfoJoystick->bStateKey[i] = KEY_UP;
				}
				break;
				case KEY_UP:
				{
					pInfoJoystick->bStateKey[i] = KEY_UP;
				}
				break;
			}
		}
	}
		
	for (int i = 0; i < Caps.NumberInputValueCaps; i++)
	{
		CHECK(
			HidP_GetUsageValue(
			HidP_Input, pValueCaps[i].UsagePage, 0, pValueCaps[i].Range.UsageMin, &value, pPreparsedData,
			(PCHAR)pRawInput->data.hid.bRawData, pRawInput->data.hid.dwSizeHid
			) == HIDP_STATUS_SUCCESS);

		switch (pValueCaps[i].Range.UsageMin)
		{
		case 0x30:	// X-axis
			pInfoJoystick->lAxisX = (LONG)value - 128;
			break;

		case 0x31:	// Y-axis
			pInfoJoystick->lAxisY = (LONG)value - 128;
			break;

		case 0x32: // Z-axis
			pInfoJoystick->lAxisZ = (LONG)value - 128;
			break;

		case 0x35: // Rotate-Z
			pInfoJoystick->lAxisRz = (LONG)value - 128;
			break;

		case 0x39:	// Hat Switch
			pInfoJoystick->lHat = value;
			break;
		}
	}
	if (pInfoJoystick->numberOfButtons != 19)//controle ps3 - o pd ja esta incluso nos demais botões !!
	{
		switch (pInfoJoystick->lHat)
		{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			{
				const int i = pInfoJoystick->numberOfButtons + pInfoJoystick->lHat;
				switch (pInfoJoystick->bStateKey[i])
				{
					case KEY_NONE:
					{
						this->onKeyDown(iIdMyJoystick + 1, i + 1);
						pInfoJoystick->bStateKey[i] = KEY_DOWN;
					}
					break;
					case KEY_DOWN:
					{
					}
					break;
					case KEY_UP:
					{
						this->onKeyDown(iIdMyJoystick + 1, i + 1);
						pInfoJoystick->bStateKey[i] = KEY_DOWN;
					}
					break;
				}
			}
			break;
			case 15:
			{
				const int j = pInfoJoystick->numberOfButtons + pInfoJoystick->lHat;
				for (int i = pInfoJoystick->numberOfButtons; i < j; i++)
				{
					switch (pInfoJoystick->bStateKey[i])
					{
						case KEY_NONE:
						{
						}
						break;
						case KEY_DOWN:
						{
							this->onKeyUp(iIdMyJoystick + 1, i + 1);
							pInfoJoystick->bStateKey[i] = KEY_UP;
						}
						break;
						case KEY_UP:
						{
							pInfoJoystick->bStateKey[i] = KEY_UP;
						}
						break;
					}
				}
			}
			break;
		}
	}
	const LONG wasMoveZeroLocal = ((pInfoJoystick->lAxisX != -1) && (pInfoJoystick->lAxisX)) || ((pInfoJoystick->lAxisY != -1) && (pInfoJoystick->lAxisY)) || pInfoJoystick->lAxisZ || pInfoJoystick->lAxisRz;
	if (wasMoveZeroLocal || pInfoJoystick->wasMoveZero)
	{
		INFO_LAST_MOVE current;
		current.lAxisX = (int)((pInfoJoystick->lAxisX == -1) ? 0 : pInfoJoystick->lAxisX);
		current.lAxisY = (int)((pInfoJoystick->lAxisY == -1) ? 0 : pInfoJoystick->lAxisY);
		current.lAxisZ = (int)((pInfoJoystick->lAxisZ == -1) ? 0 : pInfoJoystick->lAxisZ);
		current.lAxisRz = (int)((pInfoJoystick->lAxisRz == -1) ? 0 : pInfoJoystick->lAxisRz);
		if (_b_enableContinousMove)
		{
			this->onMove(iIdMyJoystick + 1,
				current.lAxisX,
				current.lAxisY,
				current.lAxisZ,
				current.lAxisRz);
			pInfoJoystick->infoLastMove = current;
		}
		else if (current.lAxisX != pInfoJoystick->infoLastMove.lAxisX ||
			current.lAxisY != pInfoJoystick->infoLastMove.lAxisY ||
			current.lAxisZ != pInfoJoystick->infoLastMove.lAxisZ ||
			current.lAxisRz != pInfoJoystick->infoLastMove.lAxisRz)
		{
			this->onMove(iIdMyJoystick + 1,
				current.lAxisX,
				current.lAxisY,
				current.lAxisZ,
				current.lAxisRz);
			pInfoJoystick->infoLastMove = current;
		}
	}
	pInfoJoystick->wasMoveZero = wasMoveZeroLocal;
	//
	// Clean up
	//
    foundJoystick = true;
Error:
	SAFE_FREE(pPreparsedData);
	SAFE_FREE(pButtonCaps);
	SAFE_FREE(pValueCaps);

    if(foundJoystick && pInfoJoystick)
    {
        for (int i= 0; i < MAX_BUTTONS; ++i)
        {
            if(infoLastMove.bButtonStates[i] != pInfoJoystick->bButtonStates[i])
                return true;
            if(infoLastMove.bStateKey[i] != pInfoJoystick->bStateKey[i])
                return true;
        }
        if(infoLastMove.lAxisRz != pInfoJoystick->lAxisRz)
            return true;
        if(infoLastMove.lAxisX != pInfoJoystick->lAxisX)
            return true;
        if(infoLastMove.lAxisY != pInfoJoystick->lAxisY)
            return true;
        if(infoLastMove.lAxisZ != pInfoJoystick->lAxisZ)
            return true;
        if(infoLastMove.lHat != pInfoJoystick->lHat)
            return true;
    }
    return false;
}
	
void JOYSTICK_LUA::registerFunctionsLuaInterface()
{
	lua_pushcfunction(lua, __luaB_print);
	lua_setglobal(lua, "print");
	lua_pushcfunction(lua, sendKeyLua);
	lua_setglobal(lua, "sendKey");
	lua_pushcfunction(lua, clickLua);
	lua_setglobal(lua, "click");
	lua_pushcfunction(lua, scrollLua);
	lua_setglobal(lua, "scroll");
	lua_pushcfunction(lua, moveMouseLua);
	lua_setglobal(lua, "moveMouse");
	lua_pushcfunction(lua, enableContinousMoveLua);
	lua_setglobal(lua, "enableContinousMove");
	lua_pushcfunction(lua, addTimerLua);
	lua_setglobal(lua, "addTimer");
}
	
void JOYSTICK_LUA::DrawButton(int i, int x, int y, BOOL bPressed)
{
    this->setPen(this->penRed);
    this->setBrush(this->whiteBrush);
	this->drawCircle(x + 15, y + 15, 15);
	char text[255] = "";
	sprintf(text, "%d", i);
	if (bPressed)
    {
        this->setBrush(this->redBrush);
		this->drawCircle(x + 15, y + 15, 15);
    }
	this->drawText(x + 10, y + 8, 0, 0, 190, text);
}
	
void JOYSTICK_LUA::DrawDPad(HDC hDC, int x, int y, LONG value)
{
    this->setPen(this->penRed);
    this->setBrush(this->redBrush);
	unsigned int centerX = 148 + 276 + 276;
	unsigned int centerY = 228;
	int xPos = (int)(sin(-2 * M_PI * value / 8 + M_PI) * 128.0);
	int yPos = (int)(cos(2 * M_PI * value / 8 + M_PI) * 128.0);
	this->drawCircle(centerX + xPos, yPos + centerY, 5);
}
	
void JOYSTICK_LUA::onKeyDown(int player,int key)
{
	lua_getglobal(this->lua, "onKeyDownJoystick");
	if (lua_isfunction(this->lua, -1))
	{
		lua_pushinteger(this->lua, player);
		lua_pushinteger(this->lua, key);
		if (lua_pcall(this->lua, 2, 0, 0))
		{
			fprintf(stderr,"[%s] ->\n[%s]", "onKeyDownJoystick", luaL_checkstring(lua, -1));
		}
	}
	else
	{
		lua_pop(this->lua, 1);
	}
}
	
void JOYSTICK_LUA::onKeyUp(int player, int key)
{
	lua_getglobal(this->lua, "onKeyUpJoystick");
	if (lua_isfunction(this->lua, -1))
	{
		lua_pushinteger(this->lua, player);
		lua_pushinteger(this->lua, key);
		if (lua_pcall(this->lua, 2, 0, 0))
		{
			fprintf(stderr, "[%s] ->\n[%s]", "onKeyUpJoystick", luaL_checkstring(lua, -1));
		}
	}
	else
	{
		lua_pop(this->lua, 1);
	}
}
	
void JOYSTICK_LUA::onMove(int player, int lx, int ly, int rx, int ry)
{
	static const float pProp_128 = 1.0f / 128.f;
	static const float pProp_127 = 1.0f / 127.f;
	lua_getglobal(this->lua, "onMoveJoystick");
	if (lua_isfunction(this->lua, -1))
	{
		const float flx = lx > 0 ? lx *  pProp_127 : lx * pProp_128;
		const float fly = ly > 0 ? ly *  pProp_127 : ly * pProp_128;

		const float frx = rx > 0 ? rx *  pProp_127 : rx * pProp_128;
		const float fry = ry > 0 ? ry *  pProp_127 : ry * pProp_128;
		lua_pushinteger(this->lua, player);
		lua_pushnumber(this->lua, flx);
		lua_pushnumber(this->lua, fly);
		lua_pushnumber(this->lua, frx);
		lua_pushnumber(this->lua, fry);
		if (lua_pcall(this->lua, 5, 0, 0))
		{
			fprintf(stderr, "[%s] ->\n[%s]", "onMoveJoystick", luaL_checkstring(lua, -1));
		}
	}
	else
	{
		lua_pop(this->lua, 1);
	}
}
	
void JOYSTICK_LUA::onInfoDevice(int player, int maxNumberButton,const char* strDeviceName,const char* extraInfo)
{
	lua_getglobal(this->lua, "onInfoJoystick");
	if (lua_isfunction(this->lua, -1))
	{
		lua_pushinteger(this->lua, player);
		lua_pushinteger(this->lua, maxNumberButton);
		lua_pushstring(this->lua, strDeviceName);
		lua_pushstring(this->lua, extraInfo);
		if (lua_pcall(this->lua, 4, 0, 0))
		{
			fprintf(stderr, "[%s] ->\n[%s]", "onInfoJoystick", luaL_checkstring(lua, -1));
		}
	}
	else
	{
		lua_pop(this->lua, 1);
	}
}
	
void JOYSTICK_LUA::onOptTryIcon(WINDOW *w, DATA_EVENT &dataEvent)
{
	JOYSTICK_LUA* j = static_cast<JOYSTICK_LUA*>(w->getObjectContext(0));
	if (j)
	{
        std::string fileNameSaved;
        const int id = dataEvent.idComponent;
		if (id == j->idTryExit)
			j->window.run = false;
		else if (id == j->idTryAbout)
		{
            wchar_t luav[255] = L"";
			wchar_t buffer[1024] = L"";
            if (JOYSTICK_LUA::isPt())
				wsprintf(buffer,
					L"Emulador de teclado/mouse através de joystick com arquivos configuráveis LUA!\n"
					L"Crie arquivos lua para interpretar seu joystick! \n"
					L"Simula teclado e / ou mouse com joystick!!! \n\n"
					L"Versao LUA: %s\n"
					L"By MBM 【ツ】", toWchar(LUA_VERSION,luav));
			else
				wsprintf(buffer,
				L"Keyboard/mouse Emulator using joystick under LUA configurable files!\n"
				L"Create LUA files to interpret your joystick! \n"
				L"Simulate keyboard and / or mouse from joystick!!! \n\n"
				L"Version LUA: %s\n"
				L"By MBM 【ツ】", toWchar(LUA_VERSION,luav));
            //w->messageBox(toChar(buffer,buffer_char));
			MessageBoxW(j->window.getHwnd(),buffer,toWchar(j->window.getNameAplication(),luav), MB_OK);
		}
		else if (id == j->idTryGettingInfo)
		{
			fileNameSaved = j->openFileSample("joystick.lua", j->getSampleGettingInformation());
		}
		else if (id == j->idTrySampleDontStarve)
		{
            fileNameSaved = j->openFileSample("DontStarve.lua", j->getSampleDontStarve());
		}
		else if (id == j->idTrySampleTobias)
		{
            fileNameSaved = j->openFileSample("Tobias.lua",j->getSampleTobias());
				
		}
        else if (id == j->idTryExample)
		{
            fileNameSaved = j->openFileSample("Example.lua",j->getSample());
				
		}
		else if (id == j->idTryDiscoverKeys)
		{
			j->modeDiscover();
		}
		else if (id == j->idTrySampleGraph)
		{
			HINSTANCE ret = ShellExecuteA(GetDesktopWindow(), "open", j->nameExe.c_str(), "demo", NULL, SW_SHOWNORMAL);
			switch ((int)ret)
			{
				//case SE_ERR_FNF:
				case ERROR_FILE_NOT_FOUND:
					if (JOYSTICK_LUA::isPt())
						j->window.messageBox("Error: %s não encontrado", j->nameExe.c_str());
					else
						j->window.messageBox("Error: %s not found!", j->nameExe.c_str());
				break;
				//case SE_ERR_PNF:
				case ERROR_PATH_NOT_FOUND:
					j->window.messageBox("Error: ERROR_PATH_NOT_FOUND em %s", j->nameExe.c_str());
				break;
				case ERROR_BAD_FORMAT:
					j->window.messageBox("Error: ERROR_BAD_FORMAT em  %s", j->nameExe.c_str());
				break;
				case SE_ERR_ACCESSDENIED:
					j->window.messageBox("Error: SE_ERR_ACCESSDENIED em %s", j->nameExe.c_str());
				break;
				case SE_ERR_ASSOCINCOMPLETE:
					j->window.messageBox("Error: SE_ERR_ASSOCINCOMPLETE em %s", j->nameExe.c_str());
				break;
				case SE_ERR_DDEBUSY:
					j->window.messageBox("Error: SE_ERR_DDEBUSY em %s", j->nameExe.c_str());
				break;
				case SE_ERR_DDEFAIL:
					j->window.messageBox("Error: SE_ERR_DDEFAIL em %s", j->nameExe.c_str());
				break;
				case SE_ERR_DDETIMEOUT:
					j->window.messageBox("Error: SE_ERR_DDETIMEOUT em %s", j->nameExe.c_str());
				break;
				case SE_ERR_DLLNOTFOUND:
					j->window.messageBox("Error: SE_ERR_DLLNOTFOUND em %s", j->nameExe.c_str());
				break;
				case SE_ERR_NOASSOC:
					j->window.messageBox("Error: SE_ERR_NOASSOC em %s", j->nameExe.c_str());
				break;
				case SE_ERR_OOM:
					j->window.messageBox("Error: SE_ERR_OOM em %s", j->nameExe.c_str());
				break;
				case SE_ERR_SHARE:
					j->window.messageBox("Error: SE_ERR_SHARE em %s", j->nameExe.c_str());
				break;
				default:
				{
					j->window.run = false;
				}
			}
		}
		else if (id == j->idTryOpenFile)
		{
            char outFileName[1024]="";
			const char* file = openFileBox("lua", "Arquivo LUA", true, false, 0, NULL,outFileName);
			if (file)
			{
				if (!j->createSceneLua(file))
				{
					if (JOYSTICK_LUA::isPt())
						w->messageBox("Falha ao executar o arquivo \n %s", file);
					else
						w->messageBox("Failed to execute the file \n %s", file);
					j->window.showConsoleWindow();
				}
			}
		}
		else if (id == j->idTryConsole)
		{
			w->showConsoleWindow();
		}
        if(fileNameSaved.size() > 0)
        {
            if (!j->createSceneLua(fileNameSaved.c_str()))
			{
				if (JOYSTICK_LUA::isPt())
					w->messageBox("Falha ao executar o arquivo \n %s", fileNameSaved.c_str());
				else
					w->messageBox("Failed to execute the file \n %s", fileNameSaved.c_str());
				j->window.showConsoleWindow();
			}
        }
	}
}
	
bool JOYSTICK_LUA::needChangeDevices(PRAWINPUTDEVICELIST pRawInputDeviceList, UINT nDevices)
{
	if (nDevices != this->lsInfoJoystick.size())
		return true;
	for (unsigned int i = 0; i < this->lsInfoJoystick.size(); i++)
	{
		INFO_JOYSTICK* pJoystick = this->lsInfoJoystick[i];
		if (pJoystick->hDevice.dwType != pRawInputDeviceList[i].dwType)
			return true;
		if (pJoystick->hDevice.hDevice != pRawInputDeviceList[i].hDevice)
			return true;
	}
	return false;
}

const char* JOYSTICK_LUA::getSample()
{
    static const char* ret = R"SAMPLE(

--Sample configuration file to the joystick 
--[[

    Help:

    CallBacks function signatures received from controls:
    function onInfoJoystick(player, maxNumberButton, name, extraInfo) 
    function onKeyDownJoystick(player, key) 
    function onKeyUpJoystick(player, key) 
    function onMoveJoystick(player, lx, ly, rx, ry) 


    Available functions to simulate keyboard/mouse: 
    sendKey(key[number or string], ['d', 'u', 'du'])->simulate a key of keyboard(d = down, u = up)
    click(key[number or string],['d', 'u', 'du'])-> mouse's click(where key is: 1 or left, 2 or right, 3 or middle)
    scroll(zoom) zoom -> boolean true ou false 
    moveMouse(x, y, relative = true) move relative  or absolute
    
    Timer function:
    addTimer(functionCallBack,time in miliseconds) Add a timer (miliseconds) to call back the function to do something.
    
    The function 'onMoveJoystick' keeps sending the motion when moving. 
    enable / disable using the function: 
    enableContinuosMove(value) (enabled by default)

--]]


timeMoveSlower = {rx = 0, ry = 0}


function timer()
    if timeMoveSlower.rx ~= 0 or timeMoveSlower.ry ~= 0 then
        moveMouse(timeMoveSlower.rx * 10,timeMoveSlower.ry * 10,true)
    end
end

addTimer(timer,200)--each 200 milisecond call 'timer' function



function onInfoJoystick(player, maxNumberButton, name, extraInfo)
    print('onInfoJoystick')
    print('player:',player)
    print('Max button(s):',maxNumberButton)
    print('Name:',name)
    print(extraInfo)
end

function onKeyDownJoystick(player, key)
    if key == 15 or key == 3 then
        click('left','d')
    end
end

function onKeyUpJoystick(player, key)
    if key == 15 or key == 3 then
        click('left','u')
    end
end

function onMoveJoystick(player, lx, ly, rx, ry)
    if lx ~= 0 or ly ~= 0 then
        timeMoveSlower.rx = 0
        timeMoveSlower.ry = 0
        moveMouse(lx,ly,true)
    elseif rx ~= 0 or ry ~= 0 then
        timeMoveSlower.rx = rx
        timeMoveSlower.ry = ry
    else
        timeMoveSlower.rx = 0
        timeMoveSlower.ry = 0
    end
end

    )SAMPLE";
    return ret;
}
	
const char* JOYSTICK_LUA::getSampleTobias()
{
	static const char* ret = R"TOBIAS(
--[[
    sample of file configuration for PS3 and or PS2  (Game: Tobias and the dark sceptres)
    #define VK_LEFT           0x25 -> 37
    #define VK_UP             0x26 -> 38
    #define VK_RIGHT          0x27 -> 39
    #define VK_DOWN           0x28 -> 40
    tab 9
--]]

print('Version',1.2)
print('Ready for the game **Tobias and the dark sceptres**')

function initPlayer()
    local Player        = {}
    Player.isPs3        = false
    Player.isPs2        = false
    Player.Select       = 1
    Player.L3           = 2
    Player.R3           = 3
    Player.Start        = 4
    Player.PadU         = 5
    Player.PadR         = 6
    Player.PadD         = 7
    Player.PadL         = 8
    Player.L2           = 9
    Player.R2           = 10
    Player.L1           = 11
    Player.R1           = 12
    Player.Triangle     = 13
    Player.Circle       = 14
    Player.X            = 15
    Player.Square       = 16
    return Player
end

controllerStatus = initPlayer()

function onInfoJoystick(player, maxNumberButton, name, extraInfo)
    print('onInfoJoystick')
    print('player:'..player)
    print('Max button(s):'..maxNumberButton)
    print('Name:'..name)
    print(extraInfo)
    if player == 1 then
        if maxNumberButton == 19 then
            controllerStatus.isPs3        = true
            controllerStatus.Select       = 1
            controllerStatus.L3           = 2
            controllerStatus.R3           = 3
            controllerStatus.Start        = 4
            controllerStatus.PadU         = 5
            controllerStatus.PadR         = 6
            controllerStatus.PadD         = 7
            controllerStatus.PadL         = 8
            controllerStatus.L2           = 9
            controllerStatus.R2           = 10
            controllerStatus.L1           = 11
            controllerStatus.R1           = 12
            controllerStatus.Triangle     = 13
            controllerStatus.Circle       = 14
            controllerStatus.X            = 15
            controllerStatus.Square       = 16
        elseif maxNumberButton == 12 then
            controllerStatus.isPs2        = true
            controllerStatus.Select       = 9
            controllerStatus.L3           = 11
            controllerStatus.R3           = 12
            controllerStatus.Start        = 10
            controllerStatus.PadU         = 13
            controllerStatus.PadR         = 15
            controllerStatus.PadD         = 17
            controllerStatus.PadL         = 19
            controllerStatus.L2           = 6
            controllerStatus.R2           = 8
            controllerStatus.L1           = 5
            controllerStatus.R1           = 7
            controllerStatus.Triangle     = 1
            controllerStatus.Circle       = 2
            controllerStatus.X            = 3
            controllerStatus.Square       = 4
        end
    else
        print("This game do not use 2 controllers")
    end
end

function onKeyDownJoystick(player, key)
    if player == 1 then
        if key == controllerStatus.PadL then
            sendKey(37,'d')
        elseif key == controllerStatus.PadU then
            sendKey(38,'d')
        elseif key == controllerStatus.PadR then
            sendKey(39,'d')
        elseif key == controllerStatus.PadD then
            sendKey(40,'d')
        elseif key == controllerStatus.X then
            sendKey('S','d')
        elseif key == controllerStatus.Circle then
            sendKey('A','d')
        elseif key == controllerStatus.Square then
            sendKey('X','d')
        elseif key == controllerStatus.Triangle then
            sendKey(9,'d')
        elseif key == controllerStatus.Start then
            sendKey('P')
        elseif key == controllerStatus.Select then
            sendKey(13)
        elseif key == controllerStatus.R1 then
            sendKey(40,'d')
        elseif key == controllerStatus.L1 then
            sendKey(49,'d')
        elseif key == controllerStatus.L2 then
            sendKey(50,'d')
        elseif key == controllerStatus.R2 then
            sendKey(51,'d')
        end
    end
end

function onKeyUpJoystick(player, key)
    if player == 1 then
        if key == controllerStatus.PadL then
            sendKey(37,'u')
        elseif key == controllerStatus.PadU then
            sendKey(38,'u')
        elseif key == controllerStatus.PadR then
            sendKey(39,'u')
        elseif key == controllerStatus.PadD then
            sendKey(40,'u')
        elseif key == controllerStatus.X then
            sendKey('S','u')
        elseif key == controllerStatus.Circle then
            sendKey('A','u')
        elseif key == controllerStatus.Square then
            sendKey('X','u')
        elseif key == controllerStatus.Triangle then
            sendKey(9,'u')
        elseif key == controllerStatus.R1 then
            sendKey(40,'u')
        elseif key == controllerStatus.L1 then
            sendKey(49,'u')
        elseif key == controllerStatus.L2 then
            sendKey(50,'u')
        elseif key == controllerStatus.R2 then
            sendKey(51,'u')
        end
    end
end

function onMoveJoystick(player, lx, ly, rx, ry)
    if player == 1 then
        if lx == 0 then
            sendKey(39,'u')
            sendKey(37,'u')
        elseif lx > 0 then
            sendKey(39,'d')
        elseif lx < 0 then
            sendKey(37,'d')
        end
    end
end

    )TOBIAS";
	return ret;
}
	
const char* JOYSTICK_LUA::getSampleDontStarve()
{
	static const char* ret = 
R"DS(
--[[

	Sample of File configuration using joystick PS3 and or PS2 for the game dontstarve

--]]

print('Version',1.2)

print('Ready for game Dont\'starve')


function initPlayer()
	local Player		= {}
	Player.isPs3		= false
	Player.isPs2		= false
	Player.Select 		= 1
	Player.L3 			= 2
	Player.R3 			= 3
	Player.Start 		= 4
	Player.PadU 		= 5
	Player.PadR 		= 6
	Player.PadD			= 7
	Player.PadL 		= 8
	Player.L2 			= 9
	Player.R2 			= 10
	Player.L1 			= 11
	Player.R1 			= 12
	Player.Trinagle 	= 13
	Player.Circle  		= 14
	Player.X 			= 15
	Player.Square 		= 16
	return Player
end

passByMoveMouseLPs3 = 0
passByMoveMouseRPs3 = 0

passUntilByMoveMouseLPs3 = 2
passUntilByMoveMouseRPs3 = 8


P1 = initPlayer()


function onInfoJoystick(player, maxNumberButton, name, extraInfo)
	print('onInfoJoystick')
	print('player:'..player) 
	print('Max button(s):'..maxNumberButton)
	print('Name:'..name)
	print(extraInfo)
	if player == 1 then
		if maxNumberButton == 19 then
			P1.isPs3 		= true
			P1.Select 		= 1
			P1.L3 			= 2
			P1.R3 			= 3
			P1.Start 		= 4
			P1.PadU 		= 5
			P1.PadR 		= 6
			P1.PadD			= 7
			P1.PadL 		= 8
			P1.L2 			= 9
			P1.R2 			= 10
			P1.L1 			= 11
			P1.R1 			= 12
			P1.Trinagle 	= 13
			P1.Circle  		= 14
			P1.X 			= 15
			P1.Square 	= 16
		elseif maxNumberButton == 12 then
			P1.isPs2 		= true
			P1.Select 		= 9
			P1.L3 			= 11
			P1.R3 			= 12
			P1.Start 		= 10
			P1.PadU 		= 13
			P1.PadR 		= 15
			P1.PadD			= 17
			P1.PadL 		= 19
			P1.L2 			= 6
			P1.R2 			= 8
			P1.L1 			= 5
			P1.R1 			= 7
			P1.Trinagle 	= 1
			P1.Circle  		= 2
			P1.X 			= 3
			P1.Square 	= 4
		end
	else
        print("This game do not use 2 controllers")
	end
end

function onKeyDownJoystick(player, key)
	if player == 1 then
		if key == P1.Square then --
			click(1)
		elseif key == P1.X then
			click(2)
		elseif key == P1.Circle then
			scroll(false)
		elseif key == P1.Trinagle then
			scroll(true)
		elseif key == P1.PadU then
			passUntilByMoveMouseLPs3 = passUntilByMoveMouseLPs3 -1
			if passUntilByMoveMouseLPs3 < 0 then passUntilByMoveMouseLPs3 = 0 end
		elseif key == P1.PadD then
			passUntilByMoveMouseLPs3 = passUntilByMoveMouseLPs3 +1
			if passUntilByMoveMouseLPs3 > 100 then passUntilByMoveMouseLPs3 = 100 end
		elseif key == P1.PadR then
			passUntilByMoveMouseRPs3 = passUntilByMoveMouseRPs3 -1
			if passUntilByMoveMouseRPs3 < 0 then passUntilByMoveMouseRPs3 = 0 end
		elseif key == P1.PadL then
			passUntilByMoveMouseRPs3 = passUntilByMoveMouseRPs3 +1
			if passUntilByMoveMouseRPs3 > 100 then passUntilByMoveMouseRPs3 = 100 end			
		elseif key == P1.Start then
			sendKey(27)--Esc
		elseif key == P1.Select then
			sendKey(9)--Tab 
		elseif key == P1.L2 then	
			sendKey('Q')
		elseif key == P1.R2 then	
			sendKey('E')
		end
	end
end

function onMoveJoystick(player, lx, ly, rx, ry)
	if player == 1 then
		passByMoveMouseLPs3 = passByMoveMouseLPs3 + 1
		if passByMoveMouseLPs3 >= passUntilByMoveMouseLPs3 then
			moveMouse(lx * 1,ly * 1)
			passByMoveMouseLPs3 = 0
		end
		
		passByMoveMouseRPs3 = passByMoveMouseRPs3 +1
		if passByMoveMouseRPs3 > passUntilByMoveMouseRPs3 then
			moveMouse(rx * 1,ry * 1)
			passByMoveMouseRPs3 = 0
		end
	end
end

)DS";

	return ret;
}
	
const char* JOYSTICK_LUA::getSampleGettingInformation()
{
	static const char* code =
		R"INFO(
--[[
	--configuration file getting information in real time
--]]


function onInfoJoystick(player, maxNumberButton, name, extraInfo)
    print('onInfoJoystick')
    print('player:',player)
    print('Max button(s):',maxNumberButton)
    print('Name:',name)
    print(extraInfo)
end

function onKeyDownJoystick(player, key)
    print('onKeyDownJoystick player:',player, 'Key:',key)
end

function onKeyUpJoystick(player, key)
    print('onKeyUpJoystick player:',player,'Key:',key)
end

function onMoveJoystick(player, lx, ly, rx, ry)
    print('onMoveJoystick player:',player,'lx:',lx,'ly:',ly,'rx:',rx,'ry:',ry)
end

        )INFO";
	return code;
}
	
bool JOYSTICK_LUA::updateDevices()
{
	UINT nDevices = 0;
	int nResult = 0;
	int iIdMyJoystick = -1;
	PRAWINPUTDEVICELIST pRawInputDeviceList;
	if (GetRawInputDeviceList(NULL, &nDevices, sizeof(RAWINPUTDEVICELIST)) != 0 || nDevices == 0)
		return false;
	pRawInputDeviceList = new RAWINPUTDEVICELIST[nDevices];
	if (GetRawInputDeviceList(pRawInputDeviceList, &nDevices, sizeof(RAWINPUTDEVICELIST)) ==  (UINT)-1)
	{
		delete[] pRawInputDeviceList;
		return false;
	}
	if (needChangeDevices(pRawInputDeviceList, nDevices))
	{
		for (unsigned int i = 0; i < this->lsInfoJoystick.size(); i++)
		{
			INFO_JOYSTICK* pJoystick = this->lsInfoJoystick[i];
			delete pJoystick;
		}
	}
	else
	{
		delete[] pRawInputDeviceList;
		return true;
	}
	this->lsInfoJoystick.clear();
	// Loop Through Device List
	for (UINT i = 0; i < nDevices; i++)
	{
		INFO_JOYSTICK* pJoystick = new INFO_JOYSTICK();
		memcpy(&pJoystick->hDevice, &pRawInputDeviceList[i], sizeof(RAWINPUTDEVICELIST));
		this->lsInfoJoystick.push_back(pJoystick);
		// Get Character Count For Device Name
		UINT nBufferSize = 0;
		nResult = GetRawInputDeviceInfoA(pRawInputDeviceList[i].hDevice, // Device
			RIDI_DEVICENAME,                // Get Device Name
			NULL,                           // NO Buff, Want Count!
			&nBufferSize);                 // Char Count Here!

		// Got Device Name?
		if (nResult < 0)
		{
			// Error
			printf("ERR: Unable to get Device Name character count.. Moving to next device.\n");

			// Next
			continue;
		}

		// Allocate Memory For Device Name
		char* strDeviceName = new char[nBufferSize + 1];

		// Get Name
		nResult = GetRawInputDeviceInfoA(pRawInputDeviceList[i].hDevice, // Device
			RIDI_DEVICENAME,                // Get Device Name
			strDeviceName,                   // Get Name!
			&nBufferSize);                 // Char Count

		// Got Device Name?
		if (nResult < 0)
		{
			// Error
			printf("ERR: Unable to get Device Name.. Moving to next device.\n");

			// Clean Up
			delete[] strDeviceName;

			// Next
			continue;
		}

		// Set Device Info & Buffer Size
		RID_DEVICE_INFO rdiDeviceInfo;
		rdiDeviceInfo.cbSize = sizeof(RID_DEVICE_INFO);
		nBufferSize = rdiDeviceInfo.cbSize;

		// Get Device Info
		nResult = GetRawInputDeviceInfoA(pRawInputDeviceList[i].hDevice,
			RIDI_DEVICEINFO,
			&rdiDeviceInfo,
			&nBufferSize);

		// Got All Buffer?
		if (nResult < 0)
		{
			// Error
			printf("ERR: Unable to read Device Info.. Moving to next device.\n");

			// Next
			continue;
		}
		if (rdiDeviceInfo.dwType == RIM_TYPEHID)
		{
			char str[255]="";
			pJoystick->name = strDeviceName;
			pJoystick->info = false;
			pJoystick->extraInfo.clear();
			sprintf(str, "Vendor Id:%ud\n", rdiDeviceInfo.hid.dwVendorId);
			pJoystick->extraInfo += str;

			sprintf(str, "Product Id:%ud\n", rdiDeviceInfo.hid.dwProductId);
			pJoystick->extraInfo += str;

			sprintf(str, "Version No:%ud\n", rdiDeviceInfo.hid.dwVersionNumber);
			pJoystick->extraInfo += str;

			sprintf(str, "Usage for the device:%ud\n", rdiDeviceInfo.hid.usUsage);
			pJoystick->extraInfo += str;

			sprintf(str, "Usage Page for the device:%ud\n", rdiDeviceInfo.hid.usUsagePage);
			pJoystick->extraInfo += str;
		}
		if (this->modeDiscoverKey && rdiDeviceInfo.dwType == RIM_TYPEKEYBOARD)
		{
			char str[255] = "";
			pJoystick->name = strDeviceName;
			pJoystick->info = false;
			pJoystick->extraInfo.clear();
			sprintf(str, "Vendor Id:%ud\n", rdiDeviceInfo.hid.dwVendorId);
			pJoystick->extraInfo += str;

			sprintf(str, "Product Id:%ud\n", rdiDeviceInfo.hid.dwProductId);
			pJoystick->extraInfo += str;

			sprintf(str, "Version No:%ud\n", rdiDeviceInfo.hid.dwVersionNumber);
			pJoystick->extraInfo += str;

			sprintf(str, "Usage for the device:%ud\n", rdiDeviceInfo.hid.usUsage);
			pJoystick->extraInfo += str;

			sprintf(str, "Usage Page for the device:%ud\n", rdiDeviceInfo.hid.usUsagePage);
			pJoystick->extraInfo += str;
		}

		// Delete Name Memory!
		delete[] strDeviceName;
	}

	// Clean Up - Free Memory
	delete[] pRawInputDeviceList;
	return true;
}
	
int JOYSTICK_LUA::onParseRawInput(WINDOW* window,HRAWINPUT phRawInput)
{
	JOYSTICK_LUA* that = static_cast<JOYSTICK_LUA*>(window->getObjectContext(0));
	if (that && that->updateDevices())
	{
		PRAWINPUT pRawInput=0;
		UINT      bufferSize=0;
		HANDLE    hHeap=0;
		const UINT ret = GetRawInputData(phRawInput, RID_INPUT, NULL, &bufferSize, sizeof(RAWINPUTHEADER));
		if (ret)
			return -1;
		hHeap = GetProcessHeap();
		pRawInput = (PRAWINPUT)HeapAlloc(hHeap, 0, bufferSize);
		if (!pRawInput)
			return -1;
		const UINT bSizeCopied = GetRawInputData(phRawInput, RID_INPUT, pRawInput, &bufferSize, sizeof(RAWINPUTHEADER));
		if (bSizeCopied != bufferSize)
			return -1;
		bool moved = that->parseRawInput(pRawInput);
		HeapFree(hHeap, 0, pRawInput);
        if(moved)
            window->refresh(1);
	}
	return 0;
}
	
void JOYSTICK_LUA::onMouseEvent(WINDOW* window, int x, int y)
{
    JOYSTICK_LUA* that = static_cast<JOYSTICK_LUA*>(window->getObjectContext(0));
	if (that)
	{
		int numDeviceValid = 0;
		for (unsigned int i = 0; i < that->lsInfoJoystick.size(); i++)
		{
			INFO_JOYSTICK* ptrInfoJoystick = that->lsInfoJoystick[i];
			if (ptrInfoJoystick->hDevice.dwType == RIM_TYPEHID)
			{
				int xx = numDeviceValid * 100;
				numDeviceValid++;
				POINT p1,p2;
				p1.x = 20 + xx;
				p1.y = 400;

				p2.x = p1.x + 100;
				p2.y = p1.y + 30;
				if (x >= p1.x && x <= p2.x &&
					y >= p1.y && y <= p2.y)
				{
					that->indexSelectedJoystick = i;
					break;
				}
			}
		}
	}
}
	
unsigned int JOYSTICK_LUA::getTotalDevices()
{
	unsigned int t = 0;
	for (unsigned int i = 0; i < this->lsInfoJoystick.size(); i++)
	{
		INFO_JOYSTICK* pInfoJoystick = this->lsInfoJoystick[i];
		if (pInfoJoystick->hDevice.dwType == RIM_TYPEHID)
			t++;
	}
	return t;
}
	
void JOYSTICK_LUA::modeDiscover()
{
	this->window.hide();
	this->window.showConsoleWindow();
	fprintf(stderr, "\n\n");
	fprintf(stderr, "Modo discover... \n pressione uma tecla para obter seu codigo correspondente... \n");
	if (!this->modeDiscoverKey)
	{
		RAWINPUTDEVICE Rid[1];
		Rid[0].usUsagePage = 0x01;
		Rid[0].usUsage = 0x06;
		Rid[0].dwFlags = RIDEV_INPUTSINK;
		Rid[0].hwndTarget = this->window.getHwnd();
		this->modeDiscoverKey = false;
		if (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) == false)
			this->window.messageBox("Falha ao registrar o teclado!");
		else
			this->modeDiscoverKey = true;
	}
}
	
std::string JOYSTICK_LUA::openFileSample(const char* defaultNameLua, const char* codeSample)
{
    char outFileName[1024]="";
	const char* file = saveFileBox("lua", JOYSTICK_LUA::isPt() ? "Criar um arquivo de exemplo!" : "Create a sample's script", true, false, 0, defaultNameLua,outFileName);
	if (file)
	{
		FILE* fp = fopen(file, "wt");
		if (fp)
		{
			fprintf(fp,"%s", codeSample);
			fclose(fp);
			if (JOYSTICK_LUA::isPt())
				this->window.messageBox("Arquivo criado com sucesso!!!\n %s", file);
			else
				this->window.messageBox("Successfully created file!!!\n %s", file);
            return std::string(file);
		}
		else
		{
			if (JOYSTICK_LUA::isPt())
				this->window.messageBox("Falha ao criar o arquivo \n %s", file);
			else
				this->window.messageBox("Failed to create the file \n %s", file);
		}
	}
    return std::string();
}
	
JOYSTICK_LUA::INFO_LAST_MOVE::INFO_LAST_MOVE()
{
	lAxisX = 0;
	lAxisY = 0;
	lAxisZ = 0;
	lAxisRz= 0;
}
	
JOYSTICK_LUA::INFO_JOYSTICK::INFO_JOYSTICK()
{
    memset(&hDevice,0,sizeof(hDevice));
	this->wasMoveZero = 0;
	this->info = false;
	memset(&bButtonStates, 0, sizeof(bButtonStates));
	memset(&lAxisX, 0, sizeof(lAxisX));
	memset(&lAxisY, 0, sizeof(lAxisY));
	memset(&lAxisZ, 0, sizeof(lAxisZ));
	memset(&lAxisRz, 0, sizeof(lAxisRz));
	memset(&lHat, 0, sizeof(lHat));
	memset(&numberOfButtons, 0, sizeof(numberOfButtons));
	memset(&bStateKey, KEY_NONE, sizeof(bStateKey));
}

bool JOYSTICK_LUA::_b_enableContinousMove = true;
JOYSTICK_LUA* JOYSTICK_LUA::myInstance = NULL;
