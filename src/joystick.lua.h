﻿/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| Copyright (C) 2015      by Michel Braz de Morais  <michel.braz.morais@gmail.com>                                       |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|                                                                                                                        |
|-----------------------------------------------------------------------------------------------------------------------*/

#ifndef LUA_JOYSTICK_WIN32_H
#define LUA_JOYSTICK_WIN32_H

#include <Windows.h>
#include <tchar.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <hidsdi.h>
#include <iostream>
#include <vector>
#include <io.h>

#define MAX_BUTTONS		128

#include <plusWindows.h>

using namespace std;
using namespace mbm;

struct lua_State;

class JOYSTICK_LUA :public DRAW
{
public:
	enum STATE_KEY
	{
		KEY_NONE,
		KEY_DOWN,
		KEY_UP
	};

	JOYSTICK_LUA();
	virtual ~JOYSTICK_LUA();

	bool render(mbm::COMPONENT_INFO & component);
    int loop();
    bool init(const bool createSampleWindow, DWORD IDI_ICON = 0L, const char* fileToLoad = NULL, const char* _nameApp = "JoystickLua.exe");
	
private:
	
    const static bool isPt();
	void DrawCrosshair(int x, int y, LONG xVal, LONG yVal);
	inline const bool createSceneLua(const char* fileToLoad);
	static int __luaB_print(lua_State *lua);
	static int moveMouseLua(lua_State *lua);
	
	static void onTimerOver(WINDOW *window, DATA_EVENT &dataEvent);
    
    void processTimer(WINDOW* window, int callBackTimer);
	static int addTimerLua(lua_State *lua);
	static int clickLua(lua_State *lua);
	static int scrollLua(lua_State *lua);
	static int enableContinousMoveLua(lua_State *lua);
	static int sendKeyLua(lua_State *lua);
	bool parseRawInput(PRAWINPUT pRawInput);
	void registerFunctionsLuaInterface();
	void DrawButton(int i, int x, int y, BOOL bPressed);
	void DrawDPad(HDC hDC, int x, int y, LONG value);
	void onKeyDown(int player,int key);
	void onKeyUp(int player, int key);
	void onMove(int player, int lx, int ly, int rx, int ry);
	void onInfoDevice(int player, int maxNumberButton,const char* strDeviceName,const char* extraInfo);
	static void onOptTryIcon(WINDOW *w, DATA_EVENT &dataEvent);
	bool needChangeDevices(PRAWINPUTDEVICELIST pRawInputDeviceList, UINT nDevices);
    const char* getSample();
	const char* getSampleTobias();
	const char* getSampleDontStarve();
	const char* getSampleGettingInformation();
	bool updateDevices();
	static int onParseRawInput(WINDOW* window,HRAWINPUT phRawInput);
	static void onMouseEvent(WINDOW* window, int x, int y);
	unsigned int getTotalDevices();
	void modeDiscover();
	std::string openFileSample(const char* defaultNameLua, const char* codeSample);
	
    struct INFO_LAST_MOVE
	{
		DWORD lAxisX;
		DWORD lAxisY;
		DWORD lAxisZ;
		DWORD lAxisRz;
		INFO_LAST_MOVE();
	};
	
    struct INFO_JOYSTICK
	{
		RAWINPUTDEVICELIST hDevice;
		BOOL bButtonStates[MAX_BUTTONS];
		STATE_KEY bStateKey[MAX_BUTTONS];
		LONG lAxisX;
		LONG lAxisY;
		LONG lAxisZ;
		LONG lAxisRz;
		LONG lHat;
		LONG wasMoveZero;
		INT  numberOfButtons;
		std::string name, extraInfo;
		bool info;
		INFO_LAST_MOVE infoLastMove;
		INFO_JOYSTICK();
	};

    struct USER_DATA_TIMER :public USER_DRAWER
    {   
        int functionCallBack,idTimer;
        bool render(COMPONENT_INFO & )
        {
            return false;
        }
    };
	lua_State* lua; 
	std::vector<INFO_JOYSTICK*> lsInfoJoystick;
	WINDOW window;
	static bool _b_enableContinousMove;
	bool modeDiscoverKey;
	int idTryAbout, idTryExit, idTryGettingInfo,idTryExample, idTryConsole, idTrySampleDontStarve, idTrySampleGraph, idTryOpenFile, idTrySampleTobias, idTryDiscoverKeys;
	unsigned int indexSelectedJoystick;
	std::string nameExe;
	std::vector<USER_DATA_TIMER*> lsTimerCallBack;
    static JOYSTICK_LUA* myInstance;
    HBRUSH whiteBrush,blueBrush,redBrush;
    HPEN penBlack,penRed,penBlue;
};

#endif